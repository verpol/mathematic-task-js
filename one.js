class Point {
	constructor (x, y) {
		this.x = x;
		this.y = y;
	}
	print () {
		return `x: ${this.x}, y: ${this.y}`;
	}
}

class Shape {
	constructor (origin) {
		this.origin = origin;
	}
	square() {};
	volume() {};
}

class Circle extends Shape {
	constructor(origin, radius) {
		super(origin);
		this.radius = radius;
	}

	square() {
		return Math.PI * this.radius * this.radius;
	}
}

class Sphere extends Shape {
	constructor (origin, radius) {
		super(origin);
		this.radius = radius;
	}
	square() {
		return 4 * Math.PI * this.radius * this.radius;
	}
	volume() {
		return 4 / 3 * Math.PI * Math.pow(this.radius, 3);
	}
}

class Rectangle extends Shape {
	constructor (origin, width, height) {
		super(origin);
		this.width = width;
		this.height = height;
	}
	square() {
		return this.width * this.height;
	}
}

class Parallelepiped extends Shape {
	constructor (origin, width, height, length) {
		super(origin);
		this.width = width;
		this.height = height;
		this.length = length;
	}
	square() {
		return 2 * (this.width * this.length + this.length * this.height + this.height * this.width);
	}
	volume() {
		return this.width * this.height * this.length;
	}
}

class Cone extends Shape {
	constructor (origin, radius, length, height) {
		super(origin);
		this.radius = radius;
		this.length = length;
		this.height = height;
	}
	square() {
		return Math.PI * this.radius * (this.length + this.radius);
	}
	volume(){
		return 1 / 3 * Math.PI * this.radius * this.radius * this.height;
	}
}

const p = new Point(2, 3);
console.log(p.print());

console.log('----------Circle----------');
const c = new Circle(new Point(0, 0), 1);
console.log(c instanceof Shape);
console.log(c instanceof Circle);
console.log('Circle square: ' + c.square());

console.log('----------Sphere----------');
const s = new Sphere(new Point(2, 4), 5);
console.log(s instanceof Shape);
console.log(s instanceof Sphere);
console.log('Sphere square: ' + s.square());
console.log('Sphere volume: ' + s.volume());

console.log('----------Rectangle----------');
const r = new Rectangle(new Point(1, 2), 5, 12);
console.log(r instanceof Shape);
console.log(r instanceof Rectangle);
console.log('Rectangle square: ' + r.square());

console.log('----------Parallelepiped----------');
const par = new Parallelepiped(new Point(2, 4), 3, 5, 7);
console.log(par instanceof Shape);
console.log(par instanceof Parallelepiped);
console.log('Parallelepiped square: ' + par.square());
console.log('Parallelepiped volume: ' + par.volume());

console.log('-------------Cone-------------');
const con = new Cone(new Point(1, 2), 4, 10, 5);
console.log(con instanceof Shape);
console.log(con instanceof Cone);
console.log('Cone square: ' + con.square());
console.log('Cone volume: ' + con.volume());