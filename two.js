class Vector2D {
	constructor(x, y){
	this.x = x;
	this.y = y;
 }
	 sum(vector) {
		return `Sum: {${this.x + vector.x}, ${this.y + vector.y}}`;
	 }
	 difference(vector) {
		 return `Difference: {${this.x - vector.x}, ${this.y - vector.y}}`;
	 }
	 scalarMultiplication(vector) {
		 return `Scalar multiplication: ${this.x * vector.x + this.y * vector.y}`;
	 }
	 turnVector(angle) {
		return `Turn vector: {${ this.x * Math.cos(angle) - this.y * Math.sin(angle) }, ${this.y * Math.cos(angle) + this.x * Math.sin(angle)}}`;
	 }
	scale(size) {
		return `Scale vector: {${this.x*size}, ${this.y*size}}`;
	}
}

class Vector3D {
	constructor(x, y, z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	sum(vector) {
		return `Sum: {${this.x + vector.x}, ${this.y + vector.y}, ${this.z + vector.z}}`;
	}
	difference(vector) {
		return `Difference: {${this.x - vector.x}, ${this.y - vector.y}, ${this.z - vector.z}}`;
	}
	scalarMultiplication(vector) {
		return `Scalar multiplication: x: ${this.x * vector.x + this.y * vector.y + this.z * vector.z}`;
	}
	vectorMultiplication(vector) {
		return `Vector multiplication: {${this.y * vector.z - this.z * vector.y}, ${this.z * vector.x - this.x * vector.z}, ${this.x * vector.y - this.y * vector.x}}`;
	}
	turnVector(angle) {
		return `Turn vector: {${ this.x }, ${ this.y * Math.cos(angle) + this.z * Math.sin(angle) }, ${ this.z * Math.cos(angle) - this.y * Math.sin(angle) }`;
	}
	scale(size) {
		return `Scale vector: {${this.x * size}, ${this.y * size}, ${this.z * size}}`;
	}
}

class VectorN {
	constructor(n, vectorCoordinates){
		for(let i=1; i<=n; i++){
			this['n'+i] = vectorCoordinates[i-1];
		}
	}
}

console.log('---------------2D Space--------------');
const vector1 = new Vector2D(5, 1);
const vector2 = new Vector2D(2, 4);
console.log(vector1.sum(vector2));
console.log(vector1.difference(vector2));
console.log(vector1.scalarMultiplication(vector2));
console.log(vector1.turnVector(Math.PI/3));
console.log(vector1.scale(3));

console.log('---------------3D Space--------------');
const vector3 = new Vector3D(3, 2, 7);
const vector4 = new Vector3D(2, 6, 5);
console.log(vector3.sum(vector4));
console.log(vector3.difference(vector4));
console.log(vector3.scalarMultiplication(vector4));
console.log(vector3.vectorMultiplication(vector4));
console.log(vector3.turnVector(Math.PI/4));
console.log(vector3.scale(4));

console.log('---------------N Space--------------');
const vector5 = new VectorN(5, [4, 2, 7, 9, 1]);
console.log(vector5);